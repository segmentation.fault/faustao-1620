#!/usr/bin/python

import sys
import os
import re
import json
import time
import traceback
import logging
import uuid

from uuid import uuid4

from pprint import pprint
from datetime import datetime, timedelta, timezone
import pytz

import telegram
from telegram import InlineQueryResultArticle, ParseMode, InputTextMessageContent, Update, InlineQueryResultCachedSticker, Bot
from telegram.ext import Updater, InlineQueryHandler, CommandHandler, CallbackContext, MessageHandler
from telegram.utils.helpers import escape_markdown
from telegram.ext.filters import Filters


import lib.utils as utils
import lib.image_utils as image_utils

logger = logging.getLogger(__name__)
sticker_cache = None

MAX_STICKERS_PER_PACK = 120

#################################
# Helpers
#################################

def initialize_logging(config):
    log_dir = os.path.join(config['app']['base-path'], config['app']['log-path'])
    log_path = os.path.join(log_dir, config['app']['log-name'])

    if not os.path.exists(log_dir):
        os.mkdir(log_dir)

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO, filename=log_path)

def load_config(path):
    return utils.load_config(path)

#################################
# Sticker creation
#################################

def load_sticker_fonts(path):
    result = {}

    images_in_path = os.listdir(path)

    for image_in_path in images_in_path:
        result[image_in_path.replace('.png', '')] = image_utils.load_png_from_path(os.path.join(path, image_in_path))

    return result

def create_sticker_image_for_timestamp(config, timestamp, fonts):
    tmpdir_path = config['app']['tmp-path']
    
    base_image = image_utils.load_png_from_path(config['telegram-bot']['sticker']['base-image-location'])
    
    timestamp_x_position = config['telegram-bot']['sticker']['timestamp-position-x']
    timestamp_y_position = config['telegram-bot']['sticker']['timestamp-position-y']
    
    sadface_x_stride = config['telegram-bot']['sticker']['sad-face-x-stride']
    sadface_y_stride = config['telegram-bot']['sticker']['sad-face-y-stride']
    
    hour_minutes_as_str = timestamp.strftime("%H:%M")
    print(hour_minutes_as_str)

    selected_images = [fonts['colon' if i == ':' else i] for i in hour_minutes_as_str]
    
    image_utils.paste_images_on_image_x(selected_images, base_image, timestamp_x_position, timestamp_y_position, stride=-1)
    image_utils.paste_images_on_image_x(fonts['face'], base_image, timestamp_x_position + sadface_x_stride + sum([x.size[0] for x in selected_images]), sadface_y_stride + timestamp_y_position)
    
    file_path = '{0}/{1}.png'.format(tmpdir_path, hour_minutes_as_str.replace(':', ''))

    base_image.save(file_path)

    return file_path

def resolve_timestamp_bounds_from_count(count):
    count_to_multiplier = int(count / MAX_STICKERS_PER_PACK)

    low = datetime.now().replace(hour=0, minute=0) + timedelta(minutes=count_to_multiplier * MAX_STICKERS_PER_PACK)
    high = datetime.now().replace(hour=0, minute=0) + timedelta(minutes=(count_to_multiplier + 1) * MAX_STICKERS_PER_PACK - 1)
    
    return low.strftime('%H:%M'), high.strftime('%H:%M')

def resolve_sticker_set_name_from_count(count):
    low_ts, high_ts = resolve_timestamp_bounds_from_count(count)

    return 'faustao1620_{0}_{1}_by_faustao1620bot'.format(low_ts, high_ts).replace(':', '')

def get_sticker_id_for_count(bot, count):
    set_name = resolve_sticker_set_name_from_count(count)
    sticker_set = get_sticker_set(bot, set_name)
    current_ids = [x.file_id for x in sticker_set.stickers]

    return current_ids[count % MAX_STICKERS_PER_PACK]

def create_sticker_pack(bot: Bot, user_id, name, title, image_path, emoji):
    with open(image_path, 'rb') as f:
        try:
            return bot.create_new_sticker_set(user_id, name, title, png_sticker=f, emojis=emoji)
        except telegram.error.BadRequest:
            return True
    
    return False

def get_sticker_set(bot: Bot, name):
    return bot.get_sticker_set(name)

def add_sticker_to_set(bot: Bot, user_id, set_name, image_path, emoji):
    with open(image_path, 'rb') as f:
        return bot.add_sticker_to_set(user_id, set_name, png_sticker=f, emojis=emoji)
    
    return False

def create_stickers(bot, config):
    set_owner_id = config['telegram-bot']['sticker']['set-owner-id']
    fonts_path = config['telegram-bot']['sticker']['fonts-location']
    sad_emoji = config['telegram-bot']['sticker']['sad-sticker-emoji']
    happy_emoji = config['telegram-bot']['sticker']['happy-sticker-emoji']
    
    fonts = load_sticker_fonts(fonts_path)

    tmpdir_path = config['app']['tmp-path']    

    if not os.path.exists(tmpdir_path):
        os.mkdir(tmpdir_path)

    timestamp = datetime.now().replace(hour=0, minute=0)

    count = 0
    current_sticker_set = None

    previous_ids = []

    while True:
        low_ts, high_ts = resolve_timestamp_bounds_from_count(count)

        emoji = happy_emoji if timestamp.hour == 16 and timestamp.minute == 20 else sad_emoji        
        file_path = create_sticker_image_for_timestamp(config, timestamp, fonts)

        if count % 120 == 0:
            current_sticker_set = 'faustao1620_{0}_{1}_by_{2}'.format(low_ts, high_ts, bot.username).replace(':', '')
            print('Creating set [{0}] with sticker [{1}]'.format(current_sticker_set, file_path))
            set_result = create_sticker_pack(bot, set_owner_id, current_sticker_set, 'faustao1620bot {0} a {1}'.format(low_ts, high_ts, file_path), file_path, emoji)
            print('Resulted in: [{0}]'.format(set_result))
            previous_ids = []
        else:
            print('Creating sticker for [{0}]'.format(file_path))
            sticker_result = add_sticker_to_set(bot, set_owner_id, current_sticker_set, file_path, emoji)
            print('Resulted in: [{0}]'.format(sticker_result))

        sticker_set = get_sticker_set(bot, current_sticker_set)
        current_ids = [x.file_id for x in sticker_set.stickers]
        
        difference = list(set(current_ids) - set(previous_ids))
        print('{0} $$ {1} $$ {2}'.format(file_path, current_sticker_set, difference[0]))
        
        previous_ids = current_ids

        if timestamp.hour == 23 and timestamp.minute == 59:
            break 
        
        count += 1
        timestamp += timedelta(minutes=1)
    
    print(bot.username)

def build_persistent_sticker_cache(bot: Bot, config):
    persistent_cache_location = config['telegram-bot']['sticker']['persistent-cache-location']

    sticker_cache = {}
    
    for minute_counter in range(1440):
        sticker_id = get_sticker_id_for_count(bot, minute_counter)
        logger.info("{0}: {1}".format(minute_counter, sticker_id))
        sticker_cache[minute_counter] = sticker_id

    with open(persistent_cache_location, "w") as f:
        json.dump(sticker_cache, f)

def load_persistent_sticker_cache(config):
    global sticker_cache
    
    persistent_cache_location = config['telegram-bot']['sticker']['persistent-cache-location']
    
    if os.path.exists(persistent_cache_location):
        with open(persistent_cache_location, 'r') as f:
            sticker_cache = json.load(f)

#################################
# Inline query
#################################

def inlinequery(update: Update, context: CallbackContext) -> None:
    current_time = datetime.now().replace(tzinfo=pytz.UTC).astimezone(pytz.timezone("America/Sao_Paulo"))
    logger.info(current_time)
    
    minute_counter = current_time.hour * 60 + current_time.minute
    
    logger.info("Received inline query and resolved counter as [{0}]".format(minute_counter))
    
    try:
        user_id = getattr(update._effective_user, 'id', 'N/A')
        first_name = getattr(update._effective_user, 'first_name', 'N/A')
        last_name = getattr(update._effective_user, 'last_name', 'N/A')
        username = getattr(update._effective_user, 'username', 'N/A')

        logger.info("Sender is user_id [{0}], first_name [{1}], last_name [{2}], username [{3}]".format(user_id, first_name, last_name, username))
    except:
        logger.info("Failed to parse user data")

    if current_time.hour == 16 and current_time.minute == 20:
        logger.info('Hardcoded sticker_file_id for 16:20')
        sticker_file_id = 'CAACAgEAAxkBAAMMYDLJj0-6oNHHnay-eySf1G5BMCgAAhcAA4dClAo8rdRndHF1Mx4E'
    elif current_time.hour == 16 and current_time.minute == 21:
        logger.info('Hardcoded sticker_file_id for 16:21')
        sticker_file_id = 'CAACAgEAAxkBAAMSYDLV2wRUisb1FI2cizEJfPRTghAAAqgEAAKHQpQKSVmPily_4mseBA'
    else:
        logger.info('Dynamic sticker_file_id')
        if sticker_cache:
            logger.info('Warm sticker cache detected, fetching from it')
            sticker_file_id = sticker_cache[str(minute_counter)]
        else:
            logger.info('Cold sticker cache detected, fetching from Telegram API')
            sticker_file_id = get_sticker_id_for_count(context.bot, minute_counter)

    logger.info("Will use sticker_file_id [{0}]".format(sticker_file_id))

    results = [
         InlineQueryResultCachedSticker(
             id=uuid4(),
             sticker_file_id=sticker_file_id
         )
    ]

    update.inline_query.answer(results, cache_time=0)

#################################
# Command handlers
#################################

def start(update, context):
    update.message.reply_text('Dummy')

def initialize_telegram_bot(updater):
    dispatcher = updater.dispatcher

    # dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(InlineQueryHandler(inlinequery))
    
    updater.start_polling()
    updater.idle()

def main():
    config = load_config(sys.argv[1])
    initialize_logging(config)

    updater = Updater(token=utils.load_telegram_api_key(config['telegram-bot']['api-key-path']), use_context=True)
    # build_persistent_sticker_cache(updater.bot, config)
    load_persistent_sticker_cache(config)
    initialize_telegram_bot(updater)
    # create_stickers(updater.bot, config)
    
if __name__ == '__main__':
    main()
