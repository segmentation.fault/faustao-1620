import yaml

def load_telegram_api_key(path):
    try:
        with open(path, 'r') as f:
            return f.readline().strip()
    except Exception as e:
        print(e)

def load_config(path):
    try:
        with open(path, 'r') as f:
            return yaml.safe_load(f)
    except Exception as e:
        print(e)

def merge_dicts(x, y):
    z = x.copy()
    z.update(y)

    return z

def is_update_from_group(update):
    return update.message.chat.type == "group"

def is_user_authorized(user_id):
    return 1

def dump_object(obj, indent = '', visited = []):
    result = ''
    identifier = id(obj)

    if hasattr(obj, '__dict__'):
        if identifier not in visited:
            visited.append(identifier)
        else:
            return '<{0} object at {1} - visited>\n'.format(obj.__class__.__name__, identifier)
        
        result += '<{0} object at {1}>\n'.format(obj.__class__.__name__, identifier)
        indent += '  '

        for key, value in obj.__dict__.items():
            result += '{0}{1}: {2}\n'.format(indent, key, '{0}'.format(dump_object(value, indent, visited)).strip())
    elif isinstance(obj, list):
        result += '\n'
        indent += '  '
        for item in obj:
            result += '{0}{1}\n'.format(indent, dump_object(item, indent, visited).strip())
    else:
        return '"{0}"'.format(obj)
    
    return result

def main():
    print("NOP")

if __name__ == '__main__':
    main()