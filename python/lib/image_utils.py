#!/usr/bin/python

from PIL import Image

def load_png_from_path(path):
    return Image.open(path)

def paste_image_on_image(img_source, img_target, anchor_x, anchor_y):
    # return img_target.paste(img_source, (anchor_x, anchor_y, anchor_x + img_source.size[0], anchor_y + img_source.size[1]), img_source)
    return img_target.alpha_composite(img_source, (anchor_x, anchor_y))

def paste_images_on_image_x(img_sources, img_target, anchor_x, anchor_y, stride = 0, reverted_y = True):
    if not isinstance(img_sources, list):
        img_sources = [ img_sources ]

    y_offset = 0    
    
    for img_source in img_sources:
        # print(img_source.size)
        # print(anchor_y)

        if reverted_y:
            y_offset = -img_source.size[1]

        paste_image_on_image(img_source, img_target, anchor_x, anchor_y + y_offset)
        anchor_x += img_source.size[0] + stride
    
    return img_target

def main():
    print("Test")
    
    image_no_number = load_png_from_path("../../res/others-no-number.png")
    test_character = load_png_from_path("../../res/font/0.png")
    colon = load_png_from_path("../../res/font/colon.png")

    pasted = paste_images_on_image_x([test_character,test_character,colon,test_character,test_character], image_no_number, 270, 85, -1)

    pasted.save('test.png')
 
if __name__ == '__main__':
    main()
