#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

BOT_CONFIG="$DIR/../res/bot-config.yml"
BOT_NAME="$DIR/../python/telegram_bot.py"
PID_FILE="$DIR/bot.pid"

echo "Starting bot: $BOT_NAME"
# python $BOT_NAME $BOT_CONFIG
nohup python $BOT_NAME $BOT_CONFIG > /dev/null 2>&1 &

BOT_PID=$!

echo "Storing pid: $BOT_PID" 
echo $BOT_PID > $PID_FILE
